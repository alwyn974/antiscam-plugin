package("dpp")
    set_homepage("https://github.com/brainboxdotcc/DPP")
    set_description("D++ Extremely Lightweight C++ Discord Library")
    set_license("Apache License 2.0")

    add_urls("https://github.com/brainboxdotcc/DPP.git")

    add_versions("v10.0.1", "fafa01257f3585d09a7e8da4e0939e7b23dfb97a")
    add_versions("v10.0.4", "25c4ff99774ddf10ad91c082712722b0b4337ea5")

    add_deps("cmake")

    set_policy("build.merge_archive", true)

    add_deps("zlib", "openssl", "libsodium", "libopus")

    on_install(function (package)
        local configs = {}
        table.insert(configs, "-DCMAKE_BUILD_TYPE="..(package:debug() and "Debug" or "Release"))
        table.insert(configs, "-DBUILD_SHARED_LIBS="..(package:config("shared") and "ON" or "OFF"))
        table.insert(configs, "-DDPP_BUILD_TEST=OFF")
        import("package.tools.cmake").install(package, configs)
    end)
