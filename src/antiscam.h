#pragma once

#include <discordframework/core/configuration.h>
#include <discordframework/utils/regex.h>
#include <map>
#include <sharedlibs-loader/plugin.h>

class AntiScam : public Plugin {
public:
  AntiScam(Bot *bot, PluginInfo *info);
  ~AntiScam();

  void onMessageCreate(const dpp::message_create_t &event);

private:
  std::shared_ptr<DiscordFramework::Configuration> m_config;
  std::vector<std::string> m_blacklist;
  std::string m_mute_msg;
  std::string m_audit_msg;

  dpp::message m_last_msg;
  int m_spam_counter;

  bool isScamMessage(std::string message);
  time_t get_next_day();
  void mute_user(dpp::snowflake guild_id, dpp::snowflake user_id);
};
